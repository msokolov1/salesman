#ifndef SALESMAN_H
#define SALESMAN_H

#include <iostream>
#include <limits>
#include <vector>
#include <map>
#include <algorithm>
#include "graph.h"
#include "Path.h"

using namespace std;

class salesman {
private:
    Graph<int> cgraph;                                // граф
    std::vector<int> vectorP;                         // Вектор решений
    std::vector<bool> available_nodes;                // Доступные для обработки узлы
    int GetNextAvailableNode();

public:
    salesman();
    salesman(Graph<int> gr);
    void SetGraph(Graph<int> gr) {
        cgraph = gr;
    }
    void Dijkstra2(Node start);
    Path* Dijkstra(Node node, Path* data);          // алгоритм Дейкстры
    Path calc(vector<int> v, Node start);        // вычисление оптимального пути
    void print_vector(vector<int> vector_to_ptint); // Выводи массив на печать
};

#endif