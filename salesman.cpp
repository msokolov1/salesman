#include "salesman.h"
#include "graph.h"

salesman::salesman() {}
salesman::salesman(Graph<int> gr) {
    cgraph = gr;
}

// мой алгоритм Дейкстры
void salesman::Dijkstra2(Node start) {
    // Задаем начальные значения
    for (int i = 0; i < cgraph.nodes.size(); i++) {
        cgraph.nodes[i].value = numeric_limits<int>::max();
    }
    start.value = 0;
    //
    for (int i = 0; i < cgraph.nodes.size(); i++) {
        vectorP.push_back(1);
        available_nodes.push_back(true);
    }
    //
    int nodeid = start.id;
    while (nodeid >= 0) {
        std::cout << std::to_string(nodeid) << std::endl;
        Node cnode = cgraph.GetNode(nodeid);
        // std::cout << "cnode.out_arc.size = " << std::to_string(cnode.out_arc.size()) << std::endl;
        for (int i = 0; i < cnode.out_arc.size(); i++) {
            Arc<int> arc = cgraph.GetArc(cnode.out_arc[i]);
            std::cout << std::to_string(arc.start_node) << "->" << std::to_string(arc.end_node) << std::endl;
            Node child_node = cgraph.GetNode(arc.end_node);
            if (child_node.value > cnode.value + arc.weight) {
                child_node.value = cnode.value + arc.weight;
                vectorP[child_node.id] = cnode.id;
                std::cout << "[" <<std::to_string(child_node.id)<< "]=" << std::to_string(cnode.id)<< std::endl;
            }
        }
        available_nodes[cnode.id] = false;
        nodeid = GetNextAvailableNode();
    }
    //
}

int salesman::GetNextAvailableNode() {
    int selected_node_id = -1;
    int min_val = numeric_limits<int>::max();
    for (int i = 0; i < cgraph.nodes.size(); i++) {
        if (available_nodes[i] && cgraph.nodes[i].value <= min_val) {
            selected_node_id = i;
            min_val = cgraph.nodes[i].value;
        }
    }
    return selected_node_id;
}

// алгоритм Дейкстры
Path* salesman::Dijkstra(Node node, Path* data) {
    //
    Graph<int> gr(-1);
    int nodes_count = cgraph.GetNodesCount();
    std::vector <std::vector<int>> matrice = cgraph.GetMatrice();
    //
    data = (Path*)calloc(nodes_count, sizeof(Path));      //пути от вершины node до других вершин графа
    int distances[nodes_count];                           //минимальное расстояние от вершины node до других
    int out[nodes_count];                                 //посещенные вершины
    // Подготовка.
    // Заполняем пути до самих себя нулями, остальные - максимальными значениями
    for (int i = 0; i < nodes_count; ++i) {
        if (i == node.id) {
            // Путь до самого себя 0
            distances[i] = 0;
            data[i] = Path(node);
        } else {
            // максимально возможное значение
            distances[i] = numeric_limits<int>::max();
        }
        // Не посещали
        out[i] = 0;
    }
    // Реализация алгоритма
    int min = numeric_limits<int>::max();
    int index = -1;
    do {
        min = numeric_limits<int>::max();
        // Ищем следующую вершину для обработки
        for (int i = 0; i < nodes_count; ++i) {
            if ((out[i] == 0) && (distances[i] < min)) {
                min = distances[i];
                index = i; // Номер обрабатываемой вершины (текущей)
            }
        }
        if (min != numeric_limits<int>::max()) {
            // Если нашли, то ..
            for (int i = 0; i < nodes_count; ++i) {
                if (matrice[index][i] > 0) {
                    // Если есть путь из текущей вершины (index)
                    // (сюда попадаем только для соседних вершин)
                    int newdistance = min + matrice[index][i]; // Вычисляем расстоянием
                    if (newdistance < distances[i]) {
                        // Если новое расстояние меньше, то записываем его и путь
                        distances[i] = newdistance;
                        // Оптимальный путь до вершины равен:
                        data[i] = data[index]; // пути до вершины, из которой пришли
                        data[i].AddNode(i, matrice[index][i]); // плюс путь между вершинами
                    }
                }
            }
            out[index] = 1; // отмечаем вершину, как отработанную
        }
    } while (min < numeric_limits<int>::max());
    // Возвращаем массив путей
    return data;
}

// вычисление оптимального пути
Path salesman::calc(vector<int> nodes_to_visit, Node start) {
    // Создаем таблицу путей.
    // Тут будут храниться все оптимальные пути из i-ой вершины в j-ую
    map<int, Path*> distances;
    // Массив путей
    Path* current_path = (Path*)calloc(cgraph.GetNodesCount(), sizeof(Path));
    // Строим пути от первой вершины до всех остальных
    current_path = Dijkstra(start, current_path);
    distances[start.id] = current_path;
    std::cout << "Dijkstra(" << start.id << ")" << std::endl;
    for (int i=0; i<cgraph.GetNodesCount();i++) {
        std::cout << "  " << i+1 << ". " << current_path[i];
    }
    // Строим пути от вершин, которые надо посетить до всех остальных
    for (size_t i = 0; i < nodes_to_visit.size(); ++i) {
        current_path = Dijkstra(nodes_to_visit[i], current_path);
        distances[nodes_to_visit[i]] = current_path;
    }
    // Строим первый маршрут. Посещаем вершины, как передали в параметре (nodes_to_visit).
    Path best_path = Path(start); // Сразу считаем его лучшим. чтобы потом сравнивать с остальными маршрутами
    sort(nodes_to_visit.begin(), nodes_to_visit.end());
    for (int i = 0; i < nodes_to_visit.size(); ++i) {
        Node last = best_path.GetLastNode();
        best_path.AddPath(distances[last.id][nodes_to_visit[i]]);
        std::cout << nodes_to_visit[i] + 1 << ". " << best_path;
    }
    // Добавляем возврат в исходную точку
    Node last = best_path.GetLastNode();
    best_path.AddPath(distances[last.id][start.id]);
    print_vector(nodes_to_visit);
    std::cout << ": " << best_path;
    //
    // Перебираем все возможные перестановки вершин, которые надо посетить (очередность их посещения)
    next_permutation(nodes_to_visit.begin(), nodes_to_visit.end());
    do {
        // Для каждой перестановки строим путь с обязательным посещением вершин с текущей очередностью в nodes_to_visit
        Path current_path = Path(start); // Текущий оптимальный путь
        for (size_t i = 0; i < nodes_to_visit.size(); ++i) {
            // Берем последний элемент из пути (это, где сейчас коммивояжер находится)
            Node last = current_path.GetLastNode();
            // И добавляем в путь оптимальный маршрут из последней точки в ту, которую необходимо посетить.
            current_path.AddPath(distances[last.id][nodes_to_visit[i]]);
            if (current_path.GetLength() > best_path.GetLength())
                // Если стоимость пути превысила текущую оптимальную, то дальше не продолжаем.
                break;
        }
        Node last = current_path.GetLastNode();
        current_path.AddPath(distances[last.id][start.id]); // Добавляем маршрут для возврата в исходную точку
        // Сравниваем получившийся маршрут с оптимальным
        if (current_path.GetLength() < best_path.GetLength()) {
            best_path = current_path;
        }
        print_vector(nodes_to_visit);
        std::cout << ": " << current_path;
    } while (next_permutation(nodes_to_visit.begin(), nodes_to_visit.end()));

    return best_path;
}

void salesman::print_vector(vector<int> vector_to_ptint) {
    std::cout <<"[";
    for (int i = 0; i < vector_to_ptint.size(); i++) {
        std::cout <<vector_to_ptint[i] + 1;
        if (i < vector_to_ptint.size() - 1) {
            std::cout <<", ";
        }
    }
    std::cout <<"]";
}


