#ifndef GRAPH_H
#define GRAPH_H

#include <map>
#include <vector>
#include <iostream>

template <typename Tweight>
struct Arc {
public:
    int id;
    int start_node;
    int end_node;
    Tweight weight;

    Arc(int id, int start, int end, Tweight weight) {
        this->id = id;
        this->start_node = start;
        this->end_node = end;
        this->weight = weight;
    }
};


struct Node {
public:
    int id;
    int value;
    std::vector <int> out_arc;

    Node(int id_)
        : id(id_)
        , out_arc() {value = 0;}
};

template <typename Tweight>
struct Graph {
public:
    // inline static int id_arc = 0;
    // inline static int id_node = 0;
    int id_arc = 0;
    int id_node = 0;
    //
    std::map <int, struct Arc<Tweight>> arcs;
    std::vector<Node> nodes;
    Tweight invalid_weight;

    Graph() 
        : arcs()
        , nodes() {}

    Graph(Tweight graph_invalid_weight)
        : arcs()
        , nodes() {invalid_weight = graph_invalid_weight;}

    // Создаем граф по матрице смежности
    Graph(std::vector <std::vector<Tweight>> matrice, Tweight zero_weight, Tweight graph_invalid_weight)
        : arcs()
        , nodes() {
        
        invalid_weight = graph_invalid_weight;
        // Создаем узлы
        for (int i = 0; i < matrice.size(); i++) {
            Node node = AddNode();
        }

        for (int i = 0; i < matrice.size(); i++) {
            std::vector<Tweight> row = matrice[i];
            Node node = GetNode(i);
            for (int j = 0; j < row.size(); j++) {
                if (row[j] != zero_weight) {
                    // Добавляем связь
                    Arc<Tweight> arc = AddArc(i, j, row[j]);
                    // Раскомментировать для отладки
                    // std::cout << std::to_string(arc.start_node) << "->" << std::to_string(arc.end_node) << "(" << std::to_string(arc.weight) <<")" << std::endl;
                }
            }
        }
    }

    Node AddNode() {
        Node new_(id_node++);
        nodes.push_back(new_);
        return new_;
    }

    Arc<Tweight> AddArc(int start, int end, Tweight weight) {
        // Проверяем, что start имеет валидный номер узла
        if (start < 0 || start >= nodes.size()) {
            return GetInvalidArc();
        }
        // Проверяем, что end имеет валидный номер узла
        if (end < 0 || end >= nodes.size()) {
            return GetInvalidArc();
        }
        Arc arc(id_arc, start, end, weight);
        (nodes[start]).out_arc.push_back(id_arc);
        arcs.insert({ id_arc, arc});
        id_arc ++;
        //arcs[id_arc++] = arc;
        //
        return arc;
    }

    Arc<Tweight> GetInvalidArc() {
        return Arc(-1, -1, -1, invalid_weight);
    }

    Node GetNode(int id) {
        if (id < 0 || id >= nodes.size()) {
            return Node(-1);
        } else {
            return nodes[id];
        }
    }

    int GetNodesCount() {
        return nodes.size();
    }

    Arc<Tweight> GetArc(int id) {
        if (id < 0 || id >= arcs.size()) {
            // Возвращаем ошибочный инстанс
            return GetInvalidArc();
        } else {
            return arcs.find(id)->second;
        }
    }

    std::vector <Node> GetConnectedNodes(int id) {
        std::vector <Node> connectedNodes;
        if (id < 0 || id >= nodes.size()) {
            return connectedNodes;
        } else {
            Node node = nodes[id];
            // Проходим по всем исходящим дугам
            for (int i = 0; i < node.out_arc.size(); i++) {
                Arc<Tweight> arc = GetArc(node.out_arc[i]);
                if (arc.end_node >= 0 && arc.end_node < nodes.size()) {
                    connectedNodes.push_back(GetNode(arc.end_node));
                }
            }
            return connectedNodes;
        }
    }

    std::vector <std::vector<Tweight>> GetMatrice() {
        std::vector <std::vector<Tweight>> matrice;
        // Инициализируем матрицу (все нули)
        for (int i = 0; i < nodes.size(); i++) {
            std::vector<int> row;
            for (int j = 0; j < nodes.size(); j++) {
                row.push_back(0);
            }
            matrice.push_back(row);
        }
        // Заполняем связи единицами
        for (int i = 0; i < nodes.size(); i++) {
            Node node = GetNode(i);
            for (int j = 0; j < node.out_arc.size(); j++) {
                Arc<Tweight> arc = GetArc(node.out_arc[j]);
                if (arc.end_node >= 0) {
                    matrice[i][arc.end_node] = arc.weight;
                }
            }
        }
        return matrice;
    } 
};

#endif
