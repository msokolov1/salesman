#include <iostream>
#include <map>
#include <vector>
#include <string>
#include "graph.h"
#include "test.h"
#include "salesman.h"

int main() 
{
    // test();
    //// Graph<int> grTest = GenerateGraph();
    // Graph<int> grTest = InputGraph();
    //
    //// PrintGraphMatrice(grTest);
    //
    //// salesman sm(grTest);
    //sm.SetGraph(grTest);
    //
    //// cout << endl;
    //// Path res = sm.calc({2, 3}, grTest.GetNode(0));
    //// cout << "Result: " << res;

    // sm.Dijkstra2(grTest.GetNode(0));
    //

    Graph<int> grTest = LoadGraphFromFile("graph2.txt");
    std::cout << "Result:" <<std::endl;
    PrintGraphMatrice(grTest);
    //
    salesman sm(grTest);
    cout << endl;
    Path res = sm.calc({2, 6}, grTest.GetNode(0));
    cout << "Result: " << res;
}
