#include "Path.h"
#include "graph.h"

Path::Path(Node start) {
    path.push_back(start);
    length = 0;
}

Path& Path::operator=(const Path& newpath) {
    path = newpath.GetPath();
    length = newpath.GetLength();
    return *this;
}

// путь
vector<Node> Path::GetPath() const { return path; }

// длина пути
int Path::GetLength() const { return length; }

// добавление вершины в конец
void Path::AddNode(Node node, int weight) {
    path.push_back(node);
    length += weight;
}

// добавление пути в конец
void Path::AddPath(Path added_path) {
    for (int i = 0; i < added_path.GetPath().size(); ++i) {
        Node last = path[path.size() - 1];
        Node added_node = added_path.GetPath()[i];
        // Проверяем, нет ли там уже такого узла
        if (last.id != added_node.id) {
            path.push_back(added_node);
        }
    }
    length += added_path.GetLength();
}
// Вывод пути в стрим
ostream& operator<<(ostream& os, const Path& p) {
    size_t n = p.GetPath().size();
    for (size_t i = 0; i < n; i++) {
        os << p.GetPath()[i].id + 1;
        if (i != n - 1) {
            os << "->";
        } else {
            os << " (";
        }
    }
    os << p.GetLength() << ')' << endl;
    return os;
}

// Получение последнего элемента пути
Node Path::GetLastNode() {
    return path[path.size() - 1];
}