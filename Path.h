#ifndef PATH_H
#define PATH_H

#include <iostream>
#include <set>
#include <vector>
#include "graph.h"

using namespace std;

class Path {
private:
    vector<Node> path;                   // путь
    int length;                         // длина пути, вычисляем динамически

public:
    Path(Node start);
    Path& operator= (const Path& newpath);
    vector<Node> GetPath() const;       // путь
    int GetLength() const;             // длина пути
    void AddNode(Node node, int weight);       // добавление вершины в конец
    void AddPath(Path added_path);             // добавление пути в конец
    Node GetLastNode();        // Последний пункт маршрута

};

ostream& operator<<(ostream& os, const Path& p);

#endif