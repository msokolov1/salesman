#ifndef TEST_H
#define TEST_H

#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include "graph.h"
#include "test.h"

void test();
Graph<int> GenerateGraph();
void PrintGraphMatrice(Graph<int> gr);
Graph<int> InputGraph();
Graph<int> LoadGraphFromFile(std::string filename);

#endif